﻿Imports System.Security.Cryptography

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim navicat As New CMBSolutions.NavicatEncrypt

            TextBox2.Text = navicat.Encrypt(TextBox1.Text)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim navicat As New CMBSolutions.NavicatEncrypt

            TextBox3.Text = navicat.Decrypt(TextBox4.Text)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            Dim navicat As New CMBSolutions.NavicatEncrypt

            Dim reg = My.Computer.Registry.CurrentUser.OpenSubKey("Software\PremiumSoft\Navicat\Servers", False)

            Dim iRow As Integer = 0
            For Each sr In reg.GetSubKeyNames
                Dim subreg = reg.OpenSubKey(sr)

                Dim t As New TextBox With {.Dock = DockStyle.Fill, .Text = subreg.GetValue("pwd"), .ReadOnly = True}

                tlp1.Controls.Add(t, 0, iRow)

                Dim t2 As New TextBox With {.Dock = DockStyle.Fill, .Text = navicat.Decrypt(subreg.GetValue("pwd")), .ReadOnly = True}

                tlp1.Controls.Add(t2, 1, iRow)

                iRow += 1
            Next
        Catch ex As Exception

        End Try
    End Sub
End Class
